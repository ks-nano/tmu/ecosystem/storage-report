import glob
import os
import pandas
import bitstring

def find_all_soc_configs(response_dir, soc):
    return [name for name in os.listdir(f"{response_dir}/{soc}") if os.path.isdir(f'{response_dir}/{soc}/{name}') and not name.startswith('.')]

def find_all_firmwares(response_dir, soc, soc_config):
    return [name for name in os.listdir(f"{response_dir}/{soc}/{soc_config}") if os.path.isdir(f'{response_dir}/{soc}/{soc_config}/{name}') and not name.startswith('.')]

def find_all_raws(response_dir, soc, soc_config, firmware):
    return [os.path.basename(f) for f in glob.glob(f"{response_dir}/{soc}/{soc_config}/{firmware}/*.raw")]

def to_tuples(iterable, tuple_size):
  return (iterable[i:i+tuple_size] for i in range(0, len(iterable), tuple_size))

# Idea
## git structure: level = dir depth
### level 1: SoCs
### level 2: SoC configuration
### level 3: firmware configuration
### level 4: captures

def parse_mfs_hil(response_dir, soc, soc_config, firmware, raw_name):
    data = {'soc': soc,
            'soc_config': soc_config,
            'firmware': firmware,
            'raw-name': raw_name}
    with open(f"{response_dir}/{soc}/{soc_config}/{firmware}/{raw_name}", 'rb') as f:
        START_MARKER = b'T%'
        END_MARKER = b'%T\n'
        tmp = f.read()
        #tmp = f.read().replace(b'\n\n',b'\n')
        hil_start_str = b'[HIL] START\r\n'
        # HIL
        data["raw_hil_config_start_pos"] = tmp.find(b'[HIL] CONFIG START')
        data["raw_hil_config_end_pos"] = tmp.find(b'[HIL] CONFIG END')
        data["raw_hil_start_pos"] = tmp.find(hil_start_str)+len(hil_start_str)
        data["raw_hil_end_pos"] = tmp.find(b'[HIL] END')
        data["raw_hil_dead_pos"] = tmp.find(b'[HIL] DEAD')
        data["raw_size"] = len(tmp)
        # HIL STATUS
        status = "Unknown"
        if data["raw_size"]==0:
            status = "UART capture empty"
        elif data["raw_hil_start_pos"]==-1:
            status = "Hil not started"
        elif data["raw_hil_dead_pos"] > 0:
            status = "Error during HIL run"
        elif data["raw_hil_end_pos"] == -1:
            status = "Hil not finished"
        elif data["raw_hil_end_pos"] > 0:
            status = "ok"
        data["raw_hil_status"] = status
        # TMU DECODING:
        tmu_transmissions = [] # splitted by isr
        if data["raw_hil_start_pos"] > 0: # parse TMU data
            total_tmu_transmitted_bytes = 0
            total_tmu_payload_bytes = 0
            total_tmu_items = 0
            #tmu_data = tmp[data["raw_hil_start_pos"]:data["raw_hil_end_pos"]]
            curr_pos = data["raw_hil_start_pos"]
            while curr_pos < data["raw_hil_end_pos"]:
                # Check block start with marker
                start = curr_pos+len(START_MARKER)
                if tmp[curr_pos:start] != START_MARKER:
                     print(tmp[curr_pos:start+10])
                     data["raw_tmu_decode_error"] = True
                     raise Exception("not impl")   
                next_start = tmp.find(START_MARKER, start)
                end = tmp.find(END_MARKER, curr_pos, next_start)
                # New block starts but last one has no end marker
                if end==-1:
                    print(repr(l))
                    data["raw_tmu_decode_error"] = True
                    raise Exception("not impl")  
                num_tmu_bytes = end-start # -start_header -end_header
                num_tmu_items = num_tmu_bytes//8
                if num_tmu_bytes/8 != float(num_tmu_bytes//8):
                    raise Exception("not impl")
                tmu_transmissions.append(tmp[start:end])
                #if len(tmp[start:end])!=11*8: #todo
                #    print(len(tmp[start:end+1]))
                #    raise a
                total_tmu_transmitted_bytes += num_tmu_bytes + len(START_MARKER) + len(END_MARKER)
                total_tmu_payload_bytes += num_tmu_bytes
                total_tmu_items += num_tmu_items
                curr_pos = end+len(END_MARKER)
            data["total_tmu_transmitted_bytes"] = total_tmu_transmitted_bytes
            data["total_tmu_payload_bytes"] = total_tmu_payload_bytes
            data["total_tmu_transactions"] = total_tmu_items
            data["tmu_transmissions"] = tmu_transmissions
            #if len(tmu_transmissions)>0:
            #    decoded_tmu_transactions_df = parse_tmu_transactions(tmu_transmissions)
            #    global buf
            #    buf = decoded_tmu_transactions_df
            #    data["tmu_decoded_transactions"] = len(decoded_tmu_transactions_df['tid'])
            #    data["tmu_decoded_interrupts"] = decoded_tmu_transactions_df['tid'].max()+1
            #    data["tmu_decoded_compression_cycles_max"] = decoded_tmu_transactions_df['cont'].max()
            #    data["tmu_decoded_compression_cycles_mean"] = decoded_tmu_transactions_df['cont'].mean()
            #    data["tmu_decoded_compression_cycles_min"] = decoded_tmu_transactions_df['cont'].min()
                #for common_isr_transactions in range(5):
                #    for t in buf
                #    write her
    return data

def parse_raw(response_dir, soc, soc_config, firmware, raw_name):
    if firmware == "mfs-hil":
        return parse_mfs_hil(response_dir, soc, soc_config, firmware, raw_name)
    raise Exception("not impl")

def parse_all_socs(response_dir, soc_names):
    data_buf = []
    for soc in soc_names:
        soc_configs = find_all_soc_configs(response_dir, soc)
        for soc_config in soc_configs:
            firmwares = find_all_firmwares(response_dir, soc, soc_config)
            for firmware in firmwares:
                raws = find_all_raws(response_dir, soc, soc_config, firmware)
                for raw in raws:
                    if 'debug' in raw:
                        print(f'Did not parse {raw}')
                        continue
                    data_buf.append(parse_raw(response_dir, soc, soc_config, firmware, raw))
    df = pandas.DataFrame(data_buf)
    return df

def parse_tmu_transactions(data, symbols=[]):
    decoded_transactions = []
    for tid, transactions in enumerate(data):
        assert len(transactions)%2==0, len(transactions)
        for qid, transaction in enumerate(to_tuples(transactions, 8)): #8 == size of one tmu transaction
            c = bitstring.BitArray(bytes=reversed(transaction), length=64)
            #address = (c[32:64].uint)
            tmp = {
            "address": (c[32:64].uint),
            "write": (c[31]),
            "size": (c[28:31].uint),
            "error": (int(c[27])),
            "cont": (c[18:27].uint),
            "cont_mode": (c[16:18].uint),
            "idle": (c[8:16].uint),
            "waitstate": (c[0:2].uint),
            "tid": tid,
            "qid": qid,
            }
            if symbols!=[]:
                for s in symbols:
                    if tmp["address"]>=s[0] and tmp["address"]<=s[1]:
                        tmp["symbol"] = s[2]
                        break
            decoded_transactions.append(tmp)
    df = pandas.DataFrame(decoded_transactions, columns=["address","write","size","error","cont","cont_mode","idle","waitstate","tid","qid","symbol"])
    return df


import subprocess
def read_symbols_from_elf(storage_dut_dir, soc, soc_config, firmware, raw_name):
    tmp = subprocess.run(["riscv32-unknown-elf-nm", "--numeric-sort", "--print-size", f"{storage_dut_dir}/{soc}/{soc_config}/{firmware}/{raw_name.replace('.raw','')}"], capture_output=True, text=True)
    assert len(tmp.stderr)==0, tmp.stderr
    result = tmp.stdout.split('\n')
    syms = []
    for r in result[0:-1]:
        decoded = r.split(' ')
        addr = int(decoded[0], 16)
        if len(decoded)==4:
            size = int(decoded[1], 16)
        else:
            size=0
        name = decoded[-1]
        syms.append((addr, addr+size, name,)) # list of addr, name
    return syms

def get_sym_by_name(symbols, name):
    tmp = [s for s in symbols if s[2]==name]
    assert len(tmp)==1, tmp
    return tmp[0]

def post_process_symbols(symbols):
    blacklist = ['__stack', '_end_stack', '_end_stack', '_stack_start']
    symbols.append((get_sym_by_name(symbols, '_end_stack')[0], get_sym_by_name(symbols, '_stack_start')[0], 'stack'),)
    #print(symbols)
    return [s for s in symbols if s[2] not in blacklist]

def decompress_row(series):
    result = []
    for i in range(0, series['cont']+1):
        tmp = {
            "write": series["write"],
            "size": series["size"],
            "error": series["error"],
            "cont": 0,
            "cont_mode": 3,
            "idle": series["idle"]//(series["cont"]+1),
            "waitstate": series["waitstate"], # todo
            "tid": series['tid'],
            "qid": series['qid'],
            "cid": i
        }
        hsizes = [1, 2, 4, 8, 16, 32]
        size_in_bytes = hsizes[series['size']]
        if series['cont_mode']==1: #inc
            tmp["address"] = series['address'] + i*size_in_bytes
        elif series['cont_mode']==2: #dec
            tmp["address"] = series['address'] - i*size_in_bytes
        else: #same(0) or none (3)
            tmp["address"] = series['address']
        result.append(tmp)
    return pandas.DataFrame(result)

# assumtions:
# Stack epilog is always visible because it is recorded after TMU is enabled!
# Stack epilog for ISR is constant! -> if access is not unique, it doesn't belong to ISR
# Stack prolog is first instr in ISR! -> last accesses belong to prolog, or we miss something + cut after isr prolog starts
def find_tmu_isr(tmu_data, tmu_enable_addr=3221228068):
    steady_data = tmu_data[tmu_data['tid']>0]
    first_transactions = steady_data[steady_data['qid']==0]['address'].unique()
    # First Action is turn tmu on!
    assert len(first_transactions) == 1
    assert first_transactions[0] == tmu_enable_addr, first_transactions
    # Now scan all epilog starts (We assume that only accesses that appear every time belong to the isr!)
    qid_max = steady_data['qid'].max()
    for i in range(1, qid_max):
        accesses = steady_data[steady_data['qid']==i]['symbol'].unique()
        if len(accesses) == 1 and accesses[0] == 'stack':
            print(f"Check qid: {i} -> belongs to ISR")
            assert len(steady_data[steady_data['qid']==i]['write'].unique()) == 1 # just to be safe: check rw is not mixed, all should be reads, because prolog code!
        else:
            print(f"qid: {i} has {len(accesses)} with {accesses} and therefore may not belong to isr")
            epilog_len = i-1
            break
    # todo assert calc bytes needs to be < riscv regs?
    index = qid_max+1 # ignore first tid
    while index < len(tmu_data):
        #index == TMU_ACTIVATE
        assert tmu_data.loc[index, 'address'] == tmu_enable_addr
        # now process epilog
        stack_words = []
        for i in range(index+1, index+epilog_len+1): # index+1 because TMU_ACTIVATE and end+1 because range doesn't include end
            decompressed_row = decompress_row(tmu_data.iloc[i])
            stack_words += list(decompressed_row['address']) # assume stack access always in words
        # now find prolog backwards
        #print(stack_words)
        prolog_starts = False
        prolog_start_idx = -1
        for i in range(index-1, index-qid_max, -1):
            #print(i)
            if tmu_data.loc[i, 'address'] in stack_words:
                prolog_starts = True
            else:
                if prolog_starts: # we already saw prolog therefore this is the end
                    prolog_start_idx = i
                    break
        assert prolog_starts, (index, qid_max, epilog_len, stack_words)
        #print(f"prolog length: {index-prolog_start_idx-1}")
        # now append informations to df
        for pro_idx in range(index-1, prolog_start_idx, -1):
            tmu_data.loc[pro_idx, 'isr'] = True
        for epi_idx in range(index, index+epilog_len+1):
            tmu_data.loc[epi_idx, 'isr'] = True
        index += qid_max+1 #uff
    #tmu_data['isr'] = tmu_data['isr'].fillna(False)
    tmu_data.fillna({'isr': False}, inplace=True)
    